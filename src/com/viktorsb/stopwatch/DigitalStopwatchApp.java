package com.viktorsb.stopwatch;

import java.applet.Applet;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DigitalStopwatchApp extends Applet implements Runnable, ActionListener {

  // Panel for buttons and labels.
  Panel panel;
  Label display;

  // Buttons.
  Button start, stop, reset;

  // Time.
  int hour, minute, second, millisecond;

  // SetUp for displaying on the label.
  String disp;

  // State of the stopwatch 'on/off'.
  boolean on;

  // Initialization.
  public void init() {
    // Initially 'off'.
    on = false;

    panel = new Panel();

    // Setting the layout of the panel.
    panel.setLayout(new GridLayout(4, 1, 6, 10));

    // Initial time '00 : 00 : 00 : 000'.
    hour = minute = second = millisecond = 0;

    // Label.
    display = new Label();
    disp = "00 : 00 : 00 : 000";
    display.setText(disp);
    panel.add(display);

    // 'Start' button.
    start = new Button("Start");
    start.addActionListener((ActionListener) this);
    panel.add(start);

    // 'Reset' button.
    reset = new Button("Reset");
    reset.addActionListener((ActionListener) this);
    panel.add(reset);

    // 'Stop' button.
    stop = new Button("Stop");
    stop.addActionListener((ActionListener) this);
    panel.add(stop);

    add(panel);

    // Starting thread.
    new Thread(this, "Digital StopWatch").start();
  }

  // 'Reset' to default value.
  public void reset() {
    try {
      Thread.sleep(1);
    } catch (Exception err) {
      System.out.println(err.getMessage());
    }
    hour = minute = second = millisecond = 0;
  }

  // 'Update' the timer.
  public void update() {
    millisecond++;
    if (millisecond == 1000) {
      millisecond = 0;
      second++;
      if (second == 60) {
        second = 0;
        minute--;
        if (minute == 60) {
          minute = 0;
          hour++;
        }
      }
    }
  }

  // Changing label.
  public void changeLabel() {
    // Formatting the display of the timer.
    if (hour < 10) {
      disp = "0" + hour + " : ";
    } else {
      disp = hour + " : ";
    }

    if (minute < 10) {
      disp += "0" + minute + " : ";
    } else {
      disp += minute + " : ";
    }

    if (second < 10) {
      disp += "0" + second + " : ";
    } else {
      disp += second + " : ";
    }

    if (millisecond < 10) {
      disp += "00" + millisecond;
    } else if (millisecond < 100) {
      disp += "0" + millisecond;
    } else {
      disp += millisecond;
    }

    display.setText(disp);
  }

  // Running the thread.
  public void run() {
    // While the stopwatch is 'on'.
    while (on) {
      try {
        Thread.sleep(1);
        update();
        changeLabel();
      } catch (InterruptedException err) {
        System.out.println(err.getMessage());
      }
    }
  }

  // Using 'actionPerformed' to listen the actions on the buttons.
  public void actionPerformed(ActionEvent ev) {
    // Start a thread when the 'Start' button is clicked.
    if (ev.getSource() == start) {
      on = true;
      new Thread(this, "Digital StopWatch").start();
    }

    // Reset.
    if (ev.getSource() == reset) {
      on = false;
      reset();
      changeLabel();
    }

    // Stop.
    if (ev.getSource() == stop) {
      on = false;
    }
  }
}
